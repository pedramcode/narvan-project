from marshmallow import fields, Schema


class MathSingleArgSchema(Schema):
    n=fields.Integer(required=True)