from marshmallow import fields, Schema


class MathDoubleArgSchema(Schema):
    n=fields.Integer(required=True)
    m=fields.Integer(required=True)