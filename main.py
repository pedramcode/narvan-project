from flask import Flask, request, Response
import settings
from schemas import MathDoubleArgSchema, MathSingleArgSchema
from _math import recursives
import json
from datetime import datetime

app = Flask(__name__)

_f = open("./help.txt", "r")
HELP_TEXT = _f.read()
_f.close()

@app.route("/math/fibonacci",methods=["POST"])
def fibonacci_endpoint():
    if request.json == None:
        return Response("Please pass data as json type. see /help", content_type="text/plain",status=400)
    json_data = request.json
    schema = MathSingleArgSchema()
    if len(schema.validate(json_data).keys()) != 0:
        return Response("Please pass correct data format. see /help", content_type="text/plain",status=400)
        
    try:
        data = schema.load(json_data)
        t1 = datetime.now()
        value = recursives.fibonacci(data['n'])
        t2 = datetime.now()
        took = (t2-t1).total_seconds()
        return Response(json.dumps({
            "result":value,
            "took":f"{took:.10f} sec"
        }),status=200, content_type="application/json")
    except Exception as exp:
        return Response(f"Error: {exp}", content_type="text/plain",status=500)

@app.route("/math/ackermann",methods=["POST"])
def ackermann_endpoint():
    if request.json == None:
        return Response("Please pass data as json type. see /help", content_type="text/plain",status=400)
    json_data = request.json
    schema = MathDoubleArgSchema()
    if len(schema.validate(json_data).keys()) != 0:
        return Response("Please pass correct data format. see /help", content_type="text/plain",status=400)
        
    try:
        data = schema.load(json_data)
        t1 = datetime.now()
        value = recursives.ackermann(data['m'],data['n'])
        t2 = datetime.now()
        took = (t2-t1).total_seconds()
        return Response(json.dumps({
            "result":value,
            "took":f"{took:.10f} sec"
        }),status=200, content_type="application/json")
    except Exception as exp:
        return Response(f"Error: {exp}", content_type="text/plain",status=500)

@app.route("/math/fact",methods=["POST"])
def fact_endpoint():
    if request.json == None:
        return Response("Please pass data as json type. see /help", content_type="text/plain",status=400)
    json_data = request.json
    schema = MathSingleArgSchema()
    if len(schema.validate(json_data).keys()) != 0:
        return Response("Please pass correct data format. see /help", content_type="text/plain",status=400)
        
    try:
        data = schema.load(json_data)
        t1 = datetime.now()
        value = recursives.fact(data['n'])
        t2 = datetime.now()
        took = (t2-t1).total_seconds()
        return Response(json.dumps({
            "result":value,
            "took":f"{took:.10f} sec"
        }),status=200, content_type="application/json")
    except Exception as exp:
        return Response(f"Error: {exp}", content_type="text/plain",status=500)

@app.route("/help",methods=["GET"])
def help_endpoint():
    return Response(HELP_TEXT,content_type="text/plain",status=200)

if __name__ == "__main__":
    app.run(settings.HOST, settings.PORT)