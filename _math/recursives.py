


def fibonacci(n:int)->int:
    """
    fibonacci(n) returns next number of fibonacci serie.
    """
    if n<=0:
        raise Exception("Invalid number, input number must be start from 1")
    elif n==1:
        return 0
    elif n==2:
        return 1
    else:
        return fibonacci(n-1)+fibonacci(n-2)

def ackermann(m:int, n:int)->int:
    """
    ackermann(m,n) returns ackermann value of "m" and "n" numbers.
    """
    if m == 0:
        return n + 1
    elif n == 0:
        return ackermann(m - 1, 1)
    else:
        return ackermann(m - 1, ackermann(m, n - 1))

def fact(n:int)->int:
    """
    fact(m,n) returns factorial of n.
    """
    if n < 1:
        return 1
    else:
        return n*fact(n-1)